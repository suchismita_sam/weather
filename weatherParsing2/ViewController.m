//
//  ViewController.m
//  weatherParsing2
//
//  Created by Click Labs133 on 11/26/15.
//  Copyright (c) 2015 Clicklabs. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()
{
    int i;
    int j;
    NSMutableArray *description;
    NSMutableArray *clouds;
    NSMutableArray *rain;
    NSMutableArray *humidity;
    NSMutableArray *main;
    NSDictionary *weatherDictionary;
   
    
}
@property (strong, nonatomic) IBOutlet UITableView *weatherData;

@end

@implementation ViewController

@synthesize weatherData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    clouds=[[NSMutableArray alloc]init];
    description=[[NSMutableArray alloc]init];
    humidity=[[NSMutableArray alloc]init];
    main=[[NSMutableArray alloc]init];
    
    [self getApi];

    // Do any additional setup after loading the view, typically from a nib.
}

-(void) getApi{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&appid=2de143494c0b295cca9337e1e96b00e0"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id json) {
             if (json) {
                 NSLog(@"%@",json);
                 
                 NSDictionary *cityDictionary=json[@"city"];
                 NSLog(@"%@",cityDictionary);
                 
                 NSDictionary *coordDictionary=json[@"coord"];
                 NSLog(@"%@",coordDictionary);
                 
                 NSArray *listArray=json[@"list"];
                 for (i=0; i<listArray.count; i++)
                 {
                     NSLog(@"%@",listArray);
                     NSDictionary *listDictionary=listArray[i];
                     NSLog(@"%@",listDictionary);
                     
                     NSNumber *cloudsDictionary=listDictionary[@"clouds"];
                     NSLog(@"clouds   %@",cloudsDictionary);
                     [clouds addObject:cloudsDictionary.stringValue];
                     
                     
                     
                     NSNumber *humidityDictionary=listDictionary[@"humidity"];
                     NSLog(@"humidity   %@",humidityDictionary);
                     
                     [humidity addObject:humidityDictionary.stringValue];
                     
                     NSNumber *pressureDictionary=listDictionary[@"pressure"];
                     NSLog(@"pressure %@",pressureDictionary);
                     
                     NSNumber *rainDictionary=listDictionary[@"rain"];
                     NSLog(@"rain %@",rainDictionary);
                     
                     NSNumber *speedDictionary=listDictionary[@"speed"];
                     NSLog(@"speed %@",speedDictionary);
                     
                     NSArray *weatherArray=listDictionary[@"weather"];
                     for (j=0; j<weatherArray.count; j++)
                     {
                         NSLog(@"%@",weatherArray);
                         
                         NSDictionary *weatherDictionary=weatherArray[j];
                         NSLog(@"%@",weatherDictionary);
                         
                         NSString *descriptionString=weatherDictionary[@"description"];
                         NSLog(@"%@",descriptionString);
                         
                         [description addObject:descriptionString];
                         
                         NSString *mainString=weatherDictionary[@"main"];
                         NSLog(@"%@",mainString);
                         
                         [main addObject:mainString];
                     }
                 }
                 
                 
                 
                 
                 [weatherData reloadData];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error.description);
             NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
         }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return clouds.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
   UILabel* label1=(UILabel *)[cell viewWithTag:100];
    label1.text=clouds[indexPath.row];
    label1.backgroundColor=[UIColor clearColor];
    
    UILabel* label2=(UILabel *)[cell viewWithTag:101];
    label2.text=description[indexPath.row];
    label2.backgroundColor=[UIColor clearColor];
    
  UILabel*  label3=(UILabel *)[cell viewWithTag:102];
    label3.text=humidity[indexPath.row];
    label3.backgroundColor=[UIColor clearColor];
    
    UILabel* label4=(UILabel *)[cell viewWithTag:103];
    label4.text=main[indexPath.row];
    label4.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
